<style>
    .address-settings dl { font-size: 1em; }
    .address-settings dt, .address-settings dd { width: auto; display: block; float: none; }
    .address-settings dd { margin-bottom: 10px; }

    .address-settings form { display: block; clear: both; float: none; margin-top: 15px; }

    .address-settings ul, .address-settings ul li { list-style: none; margin: 0; padding: 0; }
    .address-settings ul li { padding: 5px 0; }
    .address-settings form label { display: block; width: auto; }
    .address-settings form input { font-size: 1.05em; padding: 5px; }
    .address-settings form input[name="client_id"] { width: 550px; }
    .address-settings form input[name="client_secret"] { width: 250px; }
</style>
<div class="address-settings">
    <p><?= __('The Bug Genie can use %google_icon Google (%link_to_google_oauth) as an identity provider for user authentication and registration.', ['%google_icon' => image_tag('g-normal.png', ['style' => 'display: inline-block; width: 16px; vertical-align: middle; margin-left: 3px;'], false, 'oauth2_google'), '%link_to_google_oauth' => '<a href="https://developers.google.com/identity/protocols/OpenIDConnect">Google Identity Platform &raquo; OpenID Connect</a>']); ?></p>
    <p><?= __('To authenticate with the Google API, Google requires that you create a client key specific to this installation. To do this, follow the instructions under "Obtain Oauth 2.0 credentials from the Google API console", here: %link_to_google_oauth', ['%link_to_google_oauth' => link_tag('https://developers.google.com/identity/protocols/OAuth2', null, ['target' => '_blank'])]); ?></p>
    <p><?= __('When prompted, use the values listed below, then input the values of the client id and client secret here.'); ?></p>
    <dl>
        <dt>Name</dt>
        <dd><i>&lt;e.g. My Company The Bug Genie installation&gt;</i></dd>
        <dt>Authorized Javascript origins</dt>
        <dd><i style="color: #888;">Leave blank</i></dd>
        <dt>Authorized redirect URIs</dt>
        <dd><?= make_url('oauth2_google_login', [], false); ?></dd>
    </dl>
    <?php if (isset($settings_saved) && $settings_saved): ?>
        <div class="greenbox" style="margin: 5px 0px;">
            <div><?= __('Client ID and secret saved'); ?></div>
        </div>
    <?php endif; ?>
    <form action="<?= make_url('configure_oauth2_google_settings'); ?>" accept-charset="<?= \thebuggenie\core\framework\Context::getI18n()->getCharset(); ?>" method="post">
        <ul>
            <li>
                <label><?= __('Client ID'); ?></label>
                <input type="text" name="client_id" value="<?= $client_id; ?>">
            </li>
            <li>
                <label><?= __('Client secret'); ?></label>
                <input type="text" name="client_secret" value="<?= $client_secret; ?>">
            </li>
        </ul>
        <input type="submit" class="button" value="<?= __('Save'); ?>">
    </form>
</div>
