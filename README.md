# Google OpenID Connect / Oauth2.0 support
Adds Google (Google Identity Platform » OpenID Connect) as an identity provider for user authentication and registration.

## Pre-configuration
Google requires that you create a client key specific to your installation to authenticate with the Google API. 
To do this, follow the instructions under "*Obtain Oauth 2.0 credentials from the Google API console*", here: <https://developers.google.com/identity/protocols/OAuth2>

When prompted, use the values listed below:

**Name**<br>
*\<e.g. My Company The Bug Genie installation\>*<br>
**Authorized Javascript origins**<br>
*Leave blank*<br>
**Authorized redirect URIs**<br>
\<http[s]://your.thebuggenie.com\>/oauth2_google/login 

## Installation
Either install from the modules section in The Bug Genie configuration, or download and install from
[thebuggenie.com » Addons » Google authentication](http://thebuggenie.com/addons/oauth2_google)
