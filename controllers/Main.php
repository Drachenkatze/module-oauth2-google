<?php

    namespace thebuggenie\modules\oauth2_google\controllers;

    use League\OAuth2\Client\Provider\Google;
    use thebuggenie\core\entities\User;
    use thebuggenie\core\framework;
    use thebuggenie\modules\oauth2_google\Oauth2_google;

    /**
     * Main controller for the oauth2_google module
     *
     * @Routes(name_prefix="oauth2_google_", url_prefix="/oauth2_google")
     */
    class Main extends framework\Action
    {
        /**
         * Defines the oauth2 state variable within the session
         * @var string
         */
        const SESSION_OAUTH2_STATE = "oauth2state";

        /**
         * @Route(url="/login", name="login")
         * @AnonymousRoute
         *
         * @param framework\Request $request
         */
        public function runLogin(framework\Request $request)
        {
            $settings      = Oauth2_google::getModule()->getSettings();
            $client_id     = $settings['client_id'];
            $client_secret = $settings['client_secret'];

            if (!$client_id || !$client_secret) {

                // Not configured
                framework\Context::setMessage('login_error', $this->getI18n()->__('Google authentication not configured'));
                $this->forward($this->getRouting()->generate('login_page', [], false));
            }

            $provider = new Google([
                'clientId'     => $client_id,
                'clientSecret' => $client_secret,
                'redirectUri'  => $this->getRouting()->generate('oauth2_google_login', [], false)
            ]);

            if ($request->hasParameter('error')) {

                // Got an error, probably user denied access
                $this->removeSessionState();

                framework\Context::setMessage('login_error', htmlspecialchars($_GET['error'], ENT_QUOTES, 'UTF-8'));
                $this->forward($this->getRouting()->generate('login_page', [], false));

            } elseif (!$request->hasParameter('code')) {

                // If we don't have an authorization code then get one

                $authUrlOptions = [
                    'approval_prompt' => null,           // Unset as per https://github.com/thephpleague/oauth2-google/issues/42
                    "prompt" => "consent select_account" // Allow the user to confirm or select their account on re-login
                ];

                $authUrl = $provider->getAuthorizationUrl($authUrlOptions);
                $_SESSION[self::SESSION_OAUTH2_STATE] = $provider->getState();
                $this->forward($authUrl);

            } elseif (!$request->hasParameter('state') || ($request->getParameter('state') !== $_SESSION[self::SESSION_OAUTH2_STATE])) {

                // State is invalid, possible CSRF attack in progress
                $this->removeSessionState();

                framework\Context::setMessage('login_error', $this->getI18n()->__('Invalid login request'));
                $this->forward($this->getRouting()->generate('login_page', [], false));

            } else {

                $this->removeSessionState();

                // Try to get an access token (using the authorization code grant)
                $token = $provider->getAccessToken('authorization_code', [
                    'code' => $request->getParameter('code')
                ]);

                // Optional: Now you have a token you can look up a users profile data
                try {

                    // We got an access token, let's now get the owner details
                    $ownerDetails = $provider->getResourceOwner($token);
                    $user = Oauth2_google::getModule()->getOrCreateUserByOwnerDetails($ownerDetails);

                    if ($user instanceof User) {
                        // For compatibility with tbg3?
                        framework\Context::getResponse()->setCookie('tbg3_password', $user->getPassword());
                        framework\Context::getResponse()->setCookie('tbg3_username', $user->getUsername());

                        $user->setOnline();
                        $user->save();
                        $this->verifyScopeMembership($user);

                        $authentication_backend = framework\Settings::getAuthenticationBackend();
                        $token = $user->createUserSession();

                        framework\Context::setUser($user);

                        $authentication_backend->persistTokenSession($user, $token, true);


                        if (framework\Settings::get('returnfromlogin') == 'referer') {
                            $forward_url = framework\Context::getRouting()->generate('dashboard');
                        } else {
                            $forward_url = framework\Context::getRouting()->generate(framework\Settings::get('returnfromlogin'));
                        }

                        $forward_url = htmlentities($forward_url, ENT_COMPAT, framework\Context::getI18n()->getCharset());
                        return $this->forward($forward_url);
                    }

                } catch (\Exception $e) {

                    // Failed to get user details
                    framework\Context::setMessage('login_error', htmlspecialchars($e->getMessage(), ENT_QUOTES, 'UTF-8'));
                    $this->forward($this->getRouting()->generate('login_page', [], false));

                }

                $this->removeSessionState();

                framework\Context::setMessage('login_error', $this->getI18n()->__('An unknown error occurred'));
                $this->forward($this->getRouting()->generate('login_page', [], false));
            }
        }

        public function runConfigureSettings(framework\Request $request)
        {
            if ($request->isPost()) {
                Oauth2_google::getModule()->saveSetting('client_id', trim($request['client_id']));
                Oauth2_google::getModule()->saveSetting('client_secret', trim($request['client_secret']));

                framework\Context::setMessage('oauth2_google_settings_saved', true);
            }

            return $this->forward($this->getRouting()->generate('configure_module', ['config_module' => 'oauth2_google']));
        }

        protected function removeSessionState () {
            if (array_key_exists(self::SESSION_OAUTH2_STATE, $_SESSION)) {
                unset($_SESSION[self::SESSION_OAUTH2_STATE]);
            }
        }

    }

