<?php

    namespace thebuggenie\modules\oauth2_google;

    use thebuggenie\core\framework;

    /**
     * actions for the oauth2_google module
     */
    class Components extends framework\ActionComponent
    {

        public function componentSettings()
        {
            $settings = Oauth2_google::getModule()->getSettings();
            $this->client_id = $settings['client_id'];
            $this->client_secret = $settings['client_secret'];

            if (framework\Context::hasMessage('oauth2_google_settings_saved')) {
                $this->settings_saved = framework\Context::getMessageAndClear('oauth2_google_settings_saved');
            }
        }

    }

